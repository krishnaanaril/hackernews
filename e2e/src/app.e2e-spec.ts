import { browser, logging } from 'protractor';
import { AppPage } from './app.po';

describe('hacker-news-clone App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();    
  });

  it('should display title text', () => {
    // page.navigateTo();
    // expect(page.getTitleText()).toEqual('Hacker News');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    }));
  });
});
