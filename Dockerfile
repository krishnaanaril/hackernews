### STAGE 1: Build ###
FROM node:13.10-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
RUN pwd
RUN npm run build-docker

### STAGE 2: Run ###
FROM nginx:1.17.1-alpine
COPY --from=build /usr/src/app/dist/hackernews-clone /usr/share/nginx/html