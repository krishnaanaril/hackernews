import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Option } from '../../home/models/option.model';
import { StyleManagerService } from './style-manager.service';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  static storageKey = 'hn-theme-storage-current-name';

  constructor(
    private http: HttpClient,
    private styleManager: StyleManagerService
  ) {}

  getThemeOptions(): Observable<Option[]> {
    return this.http.get<Option[]>('assets/options.json');
  }

  setTheme(themeToSet) {
    this.styleManager.setStyle('theme', `assets/themes/${themeToSet}.css`);
    this.storeTheme(themeToSet);
  }

  isDarkModeEnabled(): boolean {
    if (
      window.matchMedia &&
      window.matchMedia('(prefers-color-scheme: dark)').matches
    ) {
      return true;
    }
    return false;
  }

  getDefaultTheme(): string {
    let result = this.getStoredTheme();
    if (result) {
      return result;
    }
    result = this.isDarkModeEnabled ? 'dark-theme' : 'light-theme';
    return result;
  }

  storeTheme(themeName: string) {
    try {
      window.localStorage.setItem(ThemeService.storageKey, themeName);
    } catch {}
  }

  getStoredTheme(): string {
    try {
      return window.localStorage[ThemeService.storageKey] || null;
    } catch {
      return null;
    }
  }

  clearStorage() {
    try {
      window.localStorage.removeItem(ThemeService.storageKey);
    } catch {}
  }
}
