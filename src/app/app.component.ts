import { Component, OnInit } from '@angular/core';
import { fromEvent, merge, of } from 'rxjs';
import { mapTo } from 'rxjs/operators';

import { MatSnackBar, MatSnackBarContainer } from '@angular/material/snack-bar';
import { NotificationService } from './home/service/notification.service';
import { UpdateService } from './home/service/update.service';
import { ThemeService } from './shared/service/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'hackernews-clone';
  isFirst: boolean;

  constructor(
    private notificationService: NotificationService,
    private sw: UpdateService,
    private themeService: ThemeService,
    private snackBar: MatSnackBar
  ) {
    this.sw.checkForUpdates();

    this.isFirst = true;
    merge(
      of(navigator.onLine),
      fromEvent(window, 'online').pipe(mapTo(true)),
      fromEvent(window, 'offline').pipe(mapTo(false))
    ).subscribe(
      (res: boolean) => {
        if (!res) {
          this.notificationService.open('Oops! You are offline.');
        } else if (res && !this.isFirst) {
          this.notificationService.open('Yay! You are back online.');
        }
        if (this.isFirst) {
          this.isFirst = false;
        }
      },
      (err) => {
        console.error(err);
      }
    );
  }

  ngOnInit() {
    // const currentTheme = this.themeService.getDefaultTheme();
    // this.themeService.setTheme(currentTheme);
  }
}
