import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material.module';
import { FeedComponent } from './components/feed/feed.component';
import { StoryDetailComponent } from './components/story-detail/story-detail.component';
import { StoryComponent } from './components/story/story.component';
import { SubStoryComponent } from './components/sub-story/sub-story.component';
import { HomeRoutingModule } from './home-routing.module';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [
    MainComponent,
    FeedComponent,
    StoryComponent,
    StoryDetailComponent,
    SubStoryComponent,
  ],
  imports: [CommonModule, HomeRoutingModule, HttpClientModule, MaterialModule],
})
export class HomeModule {}
