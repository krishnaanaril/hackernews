import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeedComponent } from './components/feed/feed.component';
import { StoryDetailComponent } from './components/story-detail/story-detail.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: 'home',
    component: MainComponent,
    children: [
      { path: 'feed/:feedType', component: FeedComponent },
      { path: 'detail/:itemId', component: StoryDetailComponent },
      { path: '', redirectTo: 'feed/top', pathMatch: 'full' },
    ],
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
