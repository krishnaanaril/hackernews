import { Component, Input, OnInit } from '@angular/core';
import { Item } from '../../models/item';
import { HnDataService } from '../../service/hn-data.service';

@Component({
  selector: 'app-sub-story',
  templateUrl: './sub-story.component.html',
  styleUrls: ['./sub-story.component.scss'],
})
export class SubStoryComponent implements OnInit {
  @Input() item: string;

  storyItem: Item;
  time: string;

  constructor(private service: HnDataService) {}

  ngOnInit() {
    if (this.item) {
      this.service.getItem(this.item).subscribe(
        (res) => {
          this.storyItem = res;
          // console.log(this.storyItem.text);
          if (this.storyItem.time) {
            this.time = this.service.timeSince(this.storyItem.time * 1000);
          }
        },
        (err) => {
          // console.error(err);
        },
        () => {
          // console.log('in story finally');
        }
      );
    }
  }
}
