import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HnDataService } from '../../service/hn-data.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
})
export class FeedComponent implements OnInit {
  feedType: string;
  feedItemsAll: number[];
  feedItems: number[];
  step: number;
  start: number;

  constructor(private route: ActivatedRoute, private service: HnDataService) {}

  ngOnInit() {
    this.step = 30;
    this.start = 0;
    this.route.paramMap
      .pipe(switchMap((params: ParamMap) => of(params.get('feedType'))))
      .subscribe((d) => {
        this.feedType = d;
        this.postInit();
      });
  }

  postInit() {
    this.service.getFeed(this.feedType).subscribe(
      (res) => {
        // console.log(res);
        this.feedItemsAll = res;
        // console.log(`start: ${this.start}, step: ${this.step}`);
        this.feedItems = this.feedItemsAll.slice(
          this.start,
          this.start + this.step
        );
      },
      (error) => {
        // console.error(error);
      },
      () => {
        // console.log('in finally');
      }
    );
  }

  moreClick() {
    this.start += this.step;
    // console.log(`start: ${this.start}, step: ${this.step}`);
    this.feedItems = this.feedItemsAll.slice(
      this.start,
      this.start + this.step
    );
  }

  trackByFn(index, item) {
    return item; // item.id; //index; // or
  }
}
