import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Item } from '../../models/item';
import { HnDataService } from '../../service/hn-data.service';
@Component({
  selector: 'app-story-detail',
  templateUrl: './story-detail.component.html',
  styleUrls: ['./story-detail.component.scss'],
})
export class StoryDetailComponent implements OnInit {
  itemId: string;
  currentStory: Item;
  domain: string;
  time: string;

  constructor(private route: ActivatedRoute, private service: HnDataService) {}

  ngOnInit() {
    this.route.paramMap
      .pipe(switchMap((params: ParamMap) => of(params.get('itemId'))))
      .subscribe((d) => {
        this.itemId = d;
        this.postInit();
      });
  }

  postInit() {
    this.service.getItem(this.itemId).subscribe(
      (res) => {
        this.currentStory = res;
        if (this.currentStory.url) {
          const url = new URL(this.currentStory.url);
          this.domain = url.hostname;
        }
        if (this.currentStory.time) {
          this.time = this.service.timeSince(this.currentStory.time * 1000);
        }
      },
      (err) => {
        // console.error(err);
      },
      () => {
        // console.log('in story finally');
      }
    );
  }

  trackByFn(index, item) {
    return item;
  }
}
