import { Component, Input, OnInit } from '@angular/core';
import { Item } from '../../models/item';
import { HnDataService } from '../../service/hn-data.service';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss'],
})
export class StoryComponent implements OnInit {
  @Input() item: string;
  @Input() feedType: string;
  @Input() position: number;

  storyItem: Item;
  domain: string;
  origin: string;
  favIconSource: string;
  time: string;

  constructor(private service: HnDataService) {}

  ngOnInit() {
    if (this.item) {
      this.service.getItem(this.item).subscribe(
        (res) => {
          this.storyItem = res;
          if (this.storyItem.url) {
            const url = new URL(this.storyItem.url);
            this.domain = url.hostname;
            this.origin = url.origin;
            this.time = this.service.timeSince(this.storyItem.time * 1000);
          }
        },
        (err) => {
          // console.error(err);
        },
        () => {
          // console.log('in story finally');
        }
      );
    }
  }
}
