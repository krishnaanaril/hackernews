import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Item } from '../models/item';
import { Story } from '../models/story';
@Injectable({
  providedIn: 'root',
})
export class HnDataService {
  constructor(private http: HttpClient) {}

  getFeed(feedType: string): Observable<any> {
    const url = `https://hacker-news.firebaseio.com/v0/${feedType}stories.json`;
    return this.http.get(url).pipe(retry(2));
  }

  getItem(itemId: string): Observable<Item> {
    const url = `https://hacker-news.firebaseio.com/v0/item/${itemId}.json?`;
    return this.http.get<Item>(url).pipe(retry(2));
  }

  timeSince(date: number) {
    const thisMoment = new Date().getTime();
    const seconds = Math.floor((thisMoment - date) / 1000);

    let interval = Math.floor(seconds / 31536000);

    if (interval > 0) {
      return interval + (interval === 1 ? ' year' : ' years');
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 0) {
      return interval + (interval === 1 ? ' month' : ' months');
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 0) {
      return interval + (interval === 1 ? ' day' : ' days');
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 0) {
      return interval + (interval === 1 ? ' hour' : 'hours');
    }
    interval = Math.floor(seconds / 60);
    if (interval > 0) {
      return interval + (interval === 1 ? ' minute' : ' minutes');
    }
    return Math.floor(seconds) + (interval === 1 ? ' second' : ' seconds');
  }
}
