import { TestBed } from '@angular/core/testing';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import { UpdateService } from './update.service';

describe('UpdateService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [
        ServiceWorkerModule.register('ngsw-worker.js', {
          enabled: environment.production,
        }),
      ],
    })
  );

  it('should be created', () => {
    const service: UpdateService = TestBed.inject(UpdateService);
    expect(service).toBeTruthy();
  });
});
