import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav, MatSidenavContainer } from '@angular/material/sidenav';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ThemeService } from 'src/app/shared/service/theme.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;
  @ViewChild('sidenavContainer', { static: true })
  sidenavContainer: MatSidenavContainer;
  isDarkModeEnabled: boolean;

  constructor(private router: Router, private themeService: ThemeService) {}

  ngOnInit() {
    if (this.themeService.isDarkModeEnabled) {
      this.isDarkModeEnabled = true;
    } else {
      this.isDarkModeEnabled = false;
    }
    this.sidenav.autoFocus = false;
    this.router.events
      .pipe(
        filter(
          (event): event is NavigationEnd => event instanceof NavigationEnd
        )
      )
      .subscribe((event) => {
        this.sidenav.close().then(() => {
          this.sidenavContainer.hasBackdrop = false;
        });
      });
  }

  toggleSideNav() {
    this.sidenav
      .toggle()
      .then(() => console.log('toggled'))
      .catch((err) => console.error(err));
  }

  toggleTheme() {
    if (this.isDarkModeEnabled) {
      this.isDarkModeEnabled = false;
      this.themeService.setTheme('light-theme');
    } else {
      this.isDarkModeEnabled = true;
      this.themeService.setTheme('dark-theme');
    }
  }
}
