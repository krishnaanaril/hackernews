# Hackernews - Material

This is a simple hacker news clone based on Angular PWA and material components.

## Features: 
1. Material Design
2. Uses official Hacker news [API](https://github.com/HackerNews/API).
3. This is a progressive web application.
4. CI/CD configured with Gitlab.


